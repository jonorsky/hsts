## Summary:
HSTS is a response header that informs the browser it can only connect to a certain website using HTTPS.  HSTS increases both the speed and security of HTTPS websites.

HSTS forces a site to load over HTTPS, disregarding any calls to try an HTTP connection first as in the case of 301 redirects.  This essentially sidesteps the initial HTTP load by forcing the browser to remember that this site does indeed support HTTPS. That way, the browser will load the secure version immediately and eliminates the opportunity for hackers to hijack the connection.

## Steps To Reproduce:

  1. Bash Commands

`cd /etc/nginx`

`cd /sites-availabe`

`sudo nano javacodifier.com`

  2. After that, insert add header after server { }

`add_header Strict-Transport-Security "max-age=31536000; includeSubDomains; preload" always;`

 
  3. Test **nginx** configuration

`sudo nginx -t`

  4. Reload nginx configuration 
  
  `sudo systemctl reload nginx`


## Supporting Material/References:
[list any additional material (e.g. screenshots, logs, etc.)]

  * [attachment / reference]